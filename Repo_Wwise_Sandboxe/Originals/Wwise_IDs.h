/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AMB = 1117531639U;
        static const AkUniqueID APPARITION_OBJET = 3159397271U;
        static const AkUniqueID CHATEAU_DE_SABLE = 3161842930U;
        static const AkUniqueID CLIC_MENU = 4177627398U;
        static const AkUniqueID COMBAT = 2764240573U;
        static const AkUniqueID DASH = 1942692385U;
        static const AkUniqueID DEBUT_COMBAT = 2108631708U;
        static const AkUniqueID FIGHT = 514064485U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID GAME_OVER = 1432716332U;
        static const AkUniqueID JETTE_OBJET = 2499747954U;
        static const AkUniqueID KICK = 2181839183U;
        static const AkUniqueID KO = 1719082483U;
        static const AkUniqueID LANCEMENT_PARTIE = 201215868U;
        static const AkUniqueID LET_S_GO = 4080130743U;
        static const AkUniqueID MARCUS_DONNE_COUP = 652691739U;
        static const AkUniqueID MARCUS_RECOIT_COUP = 2320170951U;
        static const AkUniqueID MARCUS_WINS = 311265788U;
        static const AkUniqueID MENU = 2607556080U;
        static const AkUniqueID OK_GO_TIMER = 171086326U;
        static const AkUniqueID PISTOLET_A_EAU = 1434562881U;
        static const AkUniqueID RAMASSE_OBJET = 2848311806U;
        static const AkUniqueID RETOUR_MENU = 1454763230U;
        static const AkUniqueID ROUND_1 = 1200082263U;
        static const AkUniqueID ROUND_2 = 1200082260U;
        static const AkUniqueID ROUND_3 = 1200082261U;
        static const AkUniqueID SEAU = 618695651U;
        static const AkUniqueID SELECT_YOUR_PLAYER = 489319307U;
        static const AkUniqueID VAL_KO = 538095055U;
        static const AkUniqueID VAL_MARCUS = 808753050U;
        static const AkUniqueID VAL_VIOLETTE = 992546569U;
        static const AkUniqueID VALIDATION_MENU = 3083747062U;
        static const AkUniqueID VICTOIRE = 881761348U;
        static const AkUniqueID VICTORY = 2716678721U;
        static const AkUniqueID VIOLETTE_DONNE_COUP = 38456922U;
        static const AkUniqueID VIOLETTE_RECOIT_COUP = 3725255040U;
        static const AkUniqueID VIOLETTE_WINS = 3889374293U;
        static const AkUniqueID YOUPI_VICTOIRE_MARCUS = 3496558949U;
        static const AkUniqueID YOUPI_VICTOIRE_VIOLETTE = 2715580098U;
    } // namespace EVENTS

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID PLAYBACK_RATE = 1524500807U;
        static const AkUniqueID RPM = 796049864U;
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN_SOUND_SANDBOXE = 4040702879U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MOTION_FACTORY_BUS = 985987111U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID DEFAULT_MOTION_DEVICE = 4230635974U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
